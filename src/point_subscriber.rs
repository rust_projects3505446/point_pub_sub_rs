use std::env;
use geometry_msgs::msg::Point;
use anyhow::{Error, Result};
fn main() -> Result<(), Error> {
    let context = rclrs::Context::new(env::args()).unwrap();
    let node = rclrs::create_node(&context, "minimal_subscriber").unwrap();
    let mut num_messages: usize = 0;
    let _subscription = node.create_subscription::<Point, _>(
        "static_point",
        rclrs::QOS_PROFILE_DEFAULT,
        move |msg: Point| {
            num_messages += 1;
            println!("I heard: \n{}\n{}\n{}", msg.x,msg.y,msg.z);
            println!("(Got {} messages so far)", num_messages);
        },
    ).unwrap();
    rclrs::spin(node).map_err(|err| err.into())
}


