use geometry_msgs::msg::Point;
/// Creates a SimplePublisherNode, initializes a node and publisher, and provides
/// methods to publish a simple "Hello World" message on a loop in separate threads.

/// Imports the Arc type from std::sync, used for thread-safe reference counting pointers,
use std::{sync::Arc, time::Duration};

struct PointPublisherNode {
    node: Arc<rclrs::Node>,
    _publisher: Arc<rclrs::Publisher<Point>>,
}
/// Creates a new SimplePublisherNode by initializing a node and publisher.
///
/// The SimplePublisherNode contains the node and publisher members.
impl PointPublisherNode {
    /// Creates a new SimplePublisherNode by initializing a node and publisher.
    ///
    /// This function takes a context and returns a Result containing the
    /// initialized SimplePublisherNode or an error. It creates a node with the
    /// given name and creates a publisher on the "publish_hello" topic.
    /// * context contains Shared state between nodes and similar entities.
    /// The SimplePublisherNode contains the node and publisher members.
    fn new(context: &rclrs::Context) -> Result<Self, rclrs::RclrsError> {
        let node = rclrs::create_node(context, "point_publisher").unwrap();
        let _publisher = node
            .create_publisher("static_point", rclrs::QOS_PROFILE_DEFAULT)
            .unwrap();
        Ok(Self { node, _publisher })
    }

    /// Publishes a "Hello World" message on the publisher.
    ///
    /// Creates a StringMsg with "Hello World" as the data, publishes it on
    /// the `_publisher`, and returns a Result. This allows regularly publishing
    /// a simple message on a loop.
    fn publish_data(&self) -> Result<(), rclrs::RclrsError> {
        let msg: Point = Point {
            x: 6.5,
            y: 6.5,
            z: 6.5,
        };
        self._publisher.publish(msg).unwrap();
        Ok(())
    }
}

/// The main function initializes a ROS 2 context, node and publisher,
/// spawns a thread to publish messages repeatedly, and spins the node
/// to receive callbacks.
///
/// It creates a context, initializes a SimplePublisherNode which creates
/// a node and publisher, clones the publisher to pass to the thread,  
/// spawns a thread to publish "Hello World" messages repeatedly, and
/// calls spin() on the node to receive callbacks. This allows publishing
/// messages asynchronously while spinning the node.
fn main() -> Result<(), rclrs::RclrsError> {
    let context = rclrs::Context::new(std::env::args()).unwrap();
    let publisher = Arc::new(PointPublisherNode::new(&context).unwrap());
    let publisher_other_thread = Arc::clone(&publisher);
    std::thread::spawn(move || -> Result<(), rclrs::RclrsError> {
        loop {
            std::thread::sleep(Duration::from_millis(1000));
            publisher_other_thread.publish_data().unwrap();
        }
    });
    rclrs::spin(publisher.node.clone())
}

